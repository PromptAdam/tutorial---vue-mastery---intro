var eventBus = new Vue()

Vue.component('product', {
	props: {
		premium: {
			type: Boolean,
			required: true
		}
	},
	template: `
		<div class="product">
			<div class="product-image">
				<img :src="image" :alt="altText">
			</div>

			<div class="product-info">
				<h1>{{ title }}</h1>
				<p v-if="inStock">In Stock</p>
				<p v-else
					:class="{ outOfStock: !inStock }">Out of Stock</p>
				<p v-show="onSale">On Sale!</p>

				<p>Colors:</p>
				<div class="color-box"
						v-for="(variant, index) in variants"
						:key="variant.variantId"
						:style="{ backgroundColor: variant.variantColor }"
						@mouseover="updateProduct(index)"
						>
				</div>

				<p>Sizes</p>
				<ul>
					<li v-for="size in sizes">{{ size }}</li>
				</ul>

				<button @click="addToCart"
								:disabled="!inStock"
								:class="{ disabledButton: !inStock }"
								>Add to cart</button>

				<button @click="removeFromCart">Remove from cart</button>

				<product-tabs :reviews="reviews" :premium="premium"></product-tabs>
			</div>

			<div class="link">
				<a :href="link">This is a link</a>
			</div>
		</div>
	`,
	data() {
		return {
			product: "Socks",
			brand: "Vue Mastery",
			variants: [
				{
					variantId: 2234,
					variantColor: "green",
					variantImage: "./assets/vmSocks-green.jpg",
					variantQuantity: 10
				},
				{
					variantId: 2235,
					variantColor: "blue",
					variantImage: "./assets/vmSocks-blue.jpg",
					variantQuantity: 5
				}
			],
			selectedVariant: 0,
			altText: "A pair of socks",
			onSale: true,
			sizes: ["S", "M", "L", "XL", "Gorilla Feet"],
			link: "https://www.google.com",
			reviews: []
		}
	},
	methods: {
		addToCart() {
			this.$emit('add-to-cart', this.variants[this.selectedVariant].variantId)
		},
		removeFromCart() {
			this.$emit('remove-from-cart', this.variants[this.selectedVariant].variantId)
		},
		updateProduct(variantIndex) {
			this.selectedVariant = variantIndex
		}
	},
	computed: {
		title() {
			return this.brand + ' ' + this.product + (this.onSale ? ' are on Sale!!!' : '')
		},
		image() {
			return this.variants[this.selectedVariant].variantImage
		},
		inStock() {
			return this.variants[this.selectedVariant].variantQuantity
		}
	},
	mounted() {
		eventBus.$on('review-submitted', productReview => {
			this.reviews.push(productReview)
		})
	}
})

Vue.component('product-details', {
	data() {
		return {
			details: [
								"80% cotton",
								"20% polyester",
								"Gender-neutral"
							]
		}
	},
	template: `
		<div>
		<p>Details:</p>
		<ul>
			<li v-for="detail in details">{{ detail }}</li>
		</ul>
		</div>
	`
})

Vue.component('product-review', {
	data() {
		return {
			name: null,
			review: null,
			rating: null,
			recommend: null,
			errors: []
		}
	},
	methods: {
		onSubmit() {
			this.errors = []
			if(this.name && this.review && this.rating && this.recommend) {
				let productReview = {
					name: this.name,
					review: this.review,
					rating: Number(this.rating),
					recommend: this.recommend
				}
				eventBus.$emit('review-submitted', productReview)
				this.name = null
				this.review = null
				this.rating = null
				this.recommend = null
			} else {
				if(!this.name) this.errors.push("Name required.")
				if(!this.review) this.errors.push("Review required.")
				if(!this.rating) this.errors.push("Rating required.")
				if(!this.recommend) this.errors.push("Recommendation required.")
			}
		}
	},
	template: `
		<form class="review-form" @submit.prevent="onSubmit">
			<p v-if="errors.length">
				<b>Please correct the following error(s):</b>
				<ul>
					<li v-for="error in errors">{{ error }}</li>
				</ul>
			</p>

			<p>
				<label for="name">Name:</label>
				<input id="name" v-model="name" placeholder="name">
			</p>

			<p>
				<label for="review">Review:</label>
				<textarea id="review" v-model="review"></textarea>
			</p>

			<p>
				<label for="rating">Rating:</label>
				<select id="rating" v-model.number="rating">
					<option>5</option>
					<option>4</option>
					<option>3</option>
					<option>2</option>
					<option>1</option>
				</select>
			</p>

			<p>
				<label for="recommend">Would you recommend this product?</label>
				<input type="radio" name="recommend" value="0" v-model="recommend">No</radio>
				<input type="radio" name="recommend" value="1" v-model="recommend" checked>Yes</radio>
			</p>

			<p>
				<input type="submit" value="Submit">
			</p>
		</form>
	`
})

Vue.component('product-shipping', {
	props: {
		premium: {
			type: Boolean,
			required: true
		}
	},
	computed: {
		shipping() {
			if (this.premium) {
				return "Free"
			} else {
				return 2.99
			}
		}
	},
	template: `
		<p>Shipping: {{ shipping }}</p>
	`
})

Vue.component('product-tabs', {
	props: {
		reviews: {
			type: Array,
			required: true
		},
		premium: {
			type: Boolean,
			required: true
		}
	},
	data() {
		return {
			tabs: ['Reviews', 'Make a Review', 'Shipping', 'Details'],
			selectedTab: 'Reviews'
		}
	},
	template: `
		<div>
			<ul>
				<span class="tab" 
							v-for="(tab, index) in tabs"
							:key="index"
							@click="selectedTab = tab"
							:class="{ activeTab: selectedTab === tab }">{{ tab }}</span>
			</ul>

			<div v-show="selectedTab === 'Reviews'">
				<p v-if="!reviews.length">There are no reviews yet.</p>
				<ul>
					<li v-for="review in reviews">
						<p>{{ review.name }}</p>
						<p>Rating: {{ review.rating }}</p>
						<p>{{ review.review }}</p>
						<p>Recommending?: {{ Number(review.recommend) ? 'Yes' : 'No' }}</p>
					</li>
				</ul>
			</div>

			<product-review v-show="selectedTab === 'Make a Review'"></product-review>

			<product-details
			v-show="selectedTab === 'Details'"></product-details>

			<product-shipping v-show="selectedTab === 'Shipping'" :premium="premium"></product-shipping>
		</div>
	`
})

var app = new Vue({
	el: '#app',
	data: {
		premium: false,
		cart: []
	},
	methods: {
		updateCart(id) {
			this.cart.push(id)
		},
		removeFromCart(id) {
			var idx = this.cart.indexOf(id)
			if (idx > -1){
				this.cart.splice(idx, 1)
			}
		}
	}
})
